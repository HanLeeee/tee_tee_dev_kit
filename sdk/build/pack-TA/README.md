# pack-TA<a name="EN_US_TOPIC_0000001078026808"></a>

-   [Introduction](#section1)
-   [Content](#section2)
-   [Usage Introduction](#section3)

## Introduction<a name="section1"></a>

The script for signing sec files.

## Content<a name="section2"></a>

 Before using the file, copy the materials required for signing the sec file<br>
 to the corresponding path.<br><br/>
The directory structure is as follows:
```
├── build_ta.sh                  # script for signing sec files
├── input                        # input path
│   ├── config.mk                # TA config.mk file
│   ├── config_ta_public.ini     # TA configuration file
│   ├── libcombine.so            # .so file obtained after TA compilation
│   ├── manifest.txt             # TA manifest.txt file
│   ├── perm_config              # signed config file
│   └── sign_sec_priv.pem        # private key used for sec file signing
├── output                       # output path
├── README.md                    # English Description
└── README_zh.md                 # Chinese Description
```

## Usage Introduction<a name="section3"></a>

1. Copy the signature key generated by keytools to the input directory
```
cp keytools/output/sign_sec_priv.pem pack-TA/input
```
2. Copy libcombine.so, manifest.txt, and config.mk to the input directory<br><br/>
3. Copy the config_ta_public.ini file to the input directory<br>
   change the path used in the config_ta_public.ini file to an absolute path<br>
```
cp test/TA/helloworld/config_ta_public.ini build/pack-TA/input
```
4. Copy the perm_config generated by the config signature to the input directory
```
cp pack-Config/output/perm_config pack-TA/input
```
5. Run the script and place the checked-out sec file in the output directory
```
./build_ta.sh
```