/*
 * Copyright (C) 2022 Huawei Technologies Co., Ltd.
 * Licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

/**
 * @file drv_io_share.h
 *
 * @brief IO操作接口
 *
 * @since 1
 */

#ifndef LIBDRV_IO_SHARED_H
#define LIBDRV_IO_SHARED_H

#include <stdint.h>

void *ioremap(uintptr_t phys_addr, unsigned long size, int32_t prot);
int32_t iounmap(uintptr_t pddr, const void *addr);

#endif
