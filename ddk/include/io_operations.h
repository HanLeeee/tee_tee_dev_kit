/*
 * Copyright (C) 2022 Huawei Technologies Co., Ltd.
 * Licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

/**
 * @file io_operation.h
 *
 * @brief IO操作接口
 *
 * @since 1
 */

#ifndef PLATDRV_IO_OPERATION_H
#define PLATDRV_IO_OPERATION_H

void read_from_io(void *to, const volatile void *from, unsigned long count);
void write_to_io(volatile void *to, const void *from, unsigned long count);

#endif
