# tee_dev_kit模块介绍<a name="ZH-CN_TOPIC_0000001078530726"></a>

-   [简介](#section469617221261)
-   [tee_dev_kit工程框架](#section15884114210197)

## 简介<a name="section469617221261"></a>
tee_dev_kit部件里提供了SDK能力，SDK能力包含：TA编译依赖的脚本、头文件，TA颁发证书并签发perm_config签名的能力，以及sec文件签名。

## tee_dev_kit工程框架<a name="section15884114210197"></a>

-   README.md：英文指导文件。
-   README_zh.md：中文指导文件。
-   sdk：tee os里的sdk包能力，详细的sdk能力介绍在sdk目录下的README.md和README_zh.md文件。
-   bundle.json: tee_dev_kit部件描述文件。
