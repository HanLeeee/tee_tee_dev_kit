# tee_dev_kit module introduction<a name="EN-US_TOPIC_0000001078530726"></a>

-   [Introduction](#section469617221261)
-   [tee_dev_kit engineering framework](#section15884114210197)

## Introduction<a name="section469617221261"></a>

The tee_dev_kit component provides SDK capabilities. The SDK capabilities include: TA compilation dependent scripts, header files, TA's ability to issue certificates and sign perm_config signatures, and sec file signatures.

## tee_dev_kit engineering framework<a name="section15884114210197"></a>

-   README.md：English guidance document.
-   README_zh.md：Chinese guidance document.
-   sdk：The sdk package capabilities in tee os, the detailed sdk capabilities are introduced in the README.md and README_zh.md files in the sdk directory.
-   bundle.json: tee_dev_kit component description file.
